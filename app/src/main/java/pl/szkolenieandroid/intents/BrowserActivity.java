package pl.szkolenieandroid.intents;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;


public class BrowserActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser);

        TextView textView = (TextView) findViewById(R.id.textView);

        String sharedText = getIntent().getStringExtra(Intent.EXTRA_TEXT);

        textView.setText("Got text: " + sharedText);
    }
}
